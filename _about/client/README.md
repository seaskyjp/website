---
service_id: 2
title: 主な取引先
short_name: 主な取引先
permalink: /about/client/
keywords: 主な取引先
icon: 
---

# 主な取引先

- パクテラ・テクノロジー・ジャパン株式会社
- 株式会社ハイシンク創研
- NECチャイナ・ソフトジャパン株式会社
- ウイングソリューションズ株式会社
